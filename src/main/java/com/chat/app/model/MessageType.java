package com.chat.app.model;

public enum MessageType {
    CHAT,
    LEAVE,
    JOIN
}
