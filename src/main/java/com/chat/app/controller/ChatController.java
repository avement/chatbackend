package com.chat.app.controller;

import com.chat.app.model.ChatMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class ChatController {
    private static final Logger logger = LogManager.getLogger(ChatController.class);

    @Value("${app.name}")
    private String appName;

    @MessageMapping("/chat.register")
    @SendTo("/topic/public")
    public ChatMessage register(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        logger.info("message in controller");
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());

        System.out.println("helloooooooooooooooo");
        return chatMessage;
    }

    @MessageMapping("/chat.send")
    @SendTo("/topic/public")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
        logger.info("message in controller1");
        logger.info("__________________"+appName);
        logger.info("message in sendMessage");
        return chatMessage;
    }
}
